﻿using Application.Extensions;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Persistence
{
    public class SeedData
    {
        public static async Task DataInitialization(AppDbContext context)
        {
            try
            {
                await SeedDataAsync<Customer>(context);
                await SeedDataAsync<Receptionist>(context);
                await context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private static async Task SeedDataAsync<TEntity>(AppDbContext context) where TEntity : class
        {
            var dbSet = context.Set<TEntity>();
            var dataExists = await dbSet.AnyAsync();
            if (!dataExists)
            {
                var data = await DataInitializer.SeedDataAsync<TEntity>();
                await dbSet.AddRangeAsync(data);
            }
        }

    }
}
