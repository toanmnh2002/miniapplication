﻿using Application.Interfaces;
using Application.Interfaces.IRepositories;
using Persistence.Repositories;

namespace Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _context;
        private readonly ICustomerRepository _customerRepository;
        private readonly IAppointmentRepository _appointmentRepository;
        private readonly IReceptionistRepository _receptionistRepository;

        public UnitOfWork(AppDbContext context)
        {
            _context = context;
            _customerRepository = new CustomerRepository(_context);
            _appointmentRepository = new AppointmentRepository(_context);
            _receptionistRepository = new ReceptionistRepository(_context);
        }

        public UnitOfWork(AppDbContext context, ICustomerRepository customerRepository, IAppointmentRepository appointmentRepository, IReceptionistRepository receptionistRepository) : this(context)
        {
            _customerRepository = customerRepository;
            _appointmentRepository = appointmentRepository;
            _receptionistRepository = receptionistRepository;
        }

        public IAppointmentRepository AppointmentRepository => _appointmentRepository;

        public ICustomerRepository CustomerRepository => _customerRepository;

        public IReceptionistRepository ReceptionistRepository => _receptionistRepository;

        public int SaveChanges() => _context.SaveChanges();

    }
}
