﻿using Application.Interfaces.IRepositories;
using Domain.Entities;

namespace Persistence.Repositories
{
    public class ReceptionistRepository : GenericRepository<Receptionist>, IReceptionistRepository
    {
        public ReceptionistRepository(AppDbContext context) : base(context)
        {
        }

        public Receptionist GetReceptionistById(int id) => _dbSet.First(x => x.Id == id);
    }
}
