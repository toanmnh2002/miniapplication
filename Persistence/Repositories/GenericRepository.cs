﻿using Application.Interfaces.IRepositories;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using System.Linq.Expressions;



namespace Persistence.Repositories
{

    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        protected DbSet<TEntity> _dbSet;

        public GenericRepository(AppDbContext context) => _dbSet = context.Set<TEntity>();

        public List<TEntity> GetAll() => _dbSet.AsNoTracking().ToList();

        public TEntity? GetById(object Id) => _dbSet.Find(Id);

        public void AddEntity(TEntity entity) => _dbSet.Add(entity);

        public void AddRange(List<TEntity> entities) => _dbSet.AddRange(entities);

        public void Update(TEntity entity) => _dbSet.Update(entity);

        public void UpdateRange(List<TEntity> entities) => _dbSet.UpdateRange(entities);

        public void Remove(TEntity entity) => _dbSet.Remove(entity);

        public void RemoveRange(List<TEntity> entities) => _dbSet.RemoveRange(entities);

        public TEntity? GetEntityByGeneric(Expression<Func<TEntity, bool>> expression, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? include = null)
        {
            IQueryable<TEntity> query = _dbSet;
            if (include is not null)
            {
                query = include(query);
            }
            return query.FirstOrDefault(expression);
        }

        public TEntity? GetEntityByCondition(Expression<Func<TEntity, bool>> expression, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? include = null)
        {
            var query = _dbSet.AsQueryable();
            if (include is not null)
            {
                query = include(query);
            }
            return query.First(expression);
        }

        public bool Any(Expression<Func<TEntity, bool>>? expression, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? include = null)
        {
            var query = _dbSet.AsQueryable();
            if (include is not null)
            {
                query = include(query);
            }
            return query.Any(expression);
        }

        public List<TEntity> Filter(Expression<Func<TEntity, bool>>? expression = null, Func<IQueryable<TEntity>, IQueryable<TEntity>>? include = null, Expression<Func<TEntity, object>>? sortColumn = null, SortDirection sortDirection = SortDirection.Descending)
        {
            var query = _dbSet.AsQueryable();

            if (include is not null)
            {
                query = include(query);
            }

            if (expression is not null)
            {
                query = query.Where(expression);
            }

            if (sortColumn is not null)
            {
                query = sortDirection == SortDirection.Ascending
                    ? query.OrderBy(sortColumn)
                    : query.OrderByDescending(sortColumn);
            }
            return query.ToList();
        }
    }

}

