﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.FluentAPIs
{
    public class ConfigurationRelationships
    {
        public class CustomerConfiguration : IEntityTypeConfiguration<Customer>
        {
            public void Configure(EntityTypeBuilder<Customer> builder)
            {
                builder.HasKey(x => x.Id);
            }
        }

        public class AppointmentConfiguration : IEntityTypeConfiguration<Appointment>
        {
            public void Configure(EntityTypeBuilder<Appointment> builder)
            {

                builder.HasKey(x => new
                {
                    x.ReceptionistId,
                    x.CustomerId
                });

                builder.HasOne(x => x.Customer)
                    .WithMany(x => x.Appointments)
                    .HasForeignKey(x => x.CustomerId);

                builder.HasOne(x => x.Receptionist)
                    .WithMany(x => x.Appointments)
                    .HasForeignKey(x => x.ReceptionistId);


            }

        }

    }
}
