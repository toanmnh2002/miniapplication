﻿using Domain.Entities;
using FluentValidation;

namespace Application.FluentValidation
{
    public class CustomerValidation : AbstractValidator<Customer>
    {
        public CustomerValidation()
        {
            //Fullname
            RuleFor(x => x.FullName)
             .NotEmpty().NotNull().WithMessage("{PropertyName} is required.")
             .Matches(@"^[a-zA-Z]+$").WithMessage("{PropertyName} is not in the correct format.")
             .MinimumLength(4);

            //Phone 
            RuleFor(p => p.Phone).NotEmpty().NotNull().WithMessage("{PropertyName} is required.")
                .MinimumLength(10).WithMessage("{PropertyName} must not be less than 10 characters.")
                .MaximumLength(10).WithMessage("{PropertyName} must not exceed 10 characters.")
                .Matches(@"^(84|0[3|5|7|8|9])+([0-9]{8})$").WithMessage("{PropertyName} is invalid");

            //Email
            RuleFor(x => x.Email)
                 .NotEmpty().NotNull().WithMessage("{PropertyName} is required.")
                 .Matches(@"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$").WithMessage("{PropertyName} is not in the correct format.")
                 .MaximumLength(100).WithMessage("{PropertyName} at most 100 characters ");

            //Purpose
            RuleFor(x => x.Purpose)
             .MinimumLength(4);
        }
    }
}
