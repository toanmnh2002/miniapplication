﻿using Domain.Entities;
using FluentValidation;

namespace Application.FluentValidation
{
    public class AppointmentValidation : AbstractValidator<Appointment>
    {
        public AppointmentValidation()
        {
            //Location
            RuleFor(x => x.Location)
             .MinimumLength(4);

            //ReceptionistId
            RuleFor(x => x.ReceptionistId)
             .NotEmpty().NotNull().WithMessage("{PropertyName} is required!")
             .ExclusiveBetween(0, int.MaxValue);

            //CustomerId
            RuleFor(x => x.CustomerId)
             .NotEmpty().NotNull().WithMessage("{PropertyName} is required!")
             .ExclusiveBetween(0, int.MaxValue);

        }
    }
}
