﻿using Domain.Entities;

namespace Application.Interfaces.IRepositories
{
    public interface ICustomerRepository : IGenericRepository<Customer>
    {
    }
}
