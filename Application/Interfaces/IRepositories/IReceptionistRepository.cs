﻿using Domain.Entities;

namespace Application.Interfaces.IRepositories
{
    public interface IReceptionistRepository : IGenericRepository<Receptionist>
    {
        Receptionist GetReceptionistById(int id);
    }
}
