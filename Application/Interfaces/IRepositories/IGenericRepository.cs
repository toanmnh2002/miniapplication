﻿using Domain.Enums;
using Microsoft.EntityFrameworkCore.Query;
using System.Linq.Expressions;

namespace Application.Interfaces.IRepositories
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        void AddEntity(TEntity entity);
        void AddRange(List<TEntity> entities);
        bool Any(Expression<Func<TEntity, bool>>? expression, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? include = null);
        List<TEntity> Filter(Expression<Func<TEntity, bool>>? expression = null, Func<IQueryable<TEntity>, IQueryable<TEntity>>? include = null, Expression<Func<TEntity, object>>? sortColumn = null, SortDirection sortDirection = SortDirection.Descending);
        List<TEntity> GetAll();
        TEntity? GetById(object Id);
        TEntity? GetEntityByCondition(Expression<Func<TEntity, bool>> expression, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? include = null);
        TEntity? GetEntityByGeneric(Expression<Func<TEntity, bool>> expression, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? include = null);
        void Remove(TEntity entity);
        void RemoveRange(List<TEntity> entities);
        void Update(TEntity entity);
        void UpdateRange(List<TEntity> entities);
    }
}