﻿using Application.Interfaces.IRepositories;

namespace Application.Interfaces
{
    public interface IUnitOfWork
    {
        IAppointmentRepository AppointmentRepository { get; }
        ICustomerRepository CustomerRepository { get; }
        IReceptionistRepository ReceptionistRepository { get; }

        int SaveChanges();
    }
}