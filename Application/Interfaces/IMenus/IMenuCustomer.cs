﻿namespace Application.Interfaces.IMenus
{
    public interface IMenuCustomer
    {
        void CustomerMenu();
        void HandleMenu();
    }
}