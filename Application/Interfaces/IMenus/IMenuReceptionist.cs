﻿namespace Application.Interfaces.IMenus
{
    public interface IMenuReceptionist
    {
        void CustomerManager();
        void HandleCustomer();
        void HandleMenu();
        void HandleSchedule();
        void ReceptionistMenu();
        void ScheduleManager();
    }
}