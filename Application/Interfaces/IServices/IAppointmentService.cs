﻿using Domain.Entities;

namespace Application.Interfaces.IServices
{
    public interface IAppointmentService
    {
        bool AddAppointment(Appointment appointment);
        IEnumerable<Appointment> ShowAllAppointment();
    }
}