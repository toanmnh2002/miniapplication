﻿using Domain.Entities;

namespace Application.Interfaces.IServices
{
    public interface IReceptionistService
    {
        Receptionist GetReceptionistById(int id);
    }
}