﻿namespace Application.Interfaces.IServices
{
    public interface IMessageProducer
    {
        void SendMessages<T>(T message);
    }
}
