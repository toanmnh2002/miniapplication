﻿using Domain.Entities;

namespace Application.Interfaces.IServices
{
    public interface ICustomerService
    {
        bool AddCustomer(Customer customer);
        bool DeleteCustomer(Customer customer);
        bool UpdateCustomer(Customer customer);
        Customer GetCustomerById(int id);
        IEnumerable<Customer> ShowAllCustomer();
        IEnumerable<Customer> ShowAppointmentCustomer();
    }
}