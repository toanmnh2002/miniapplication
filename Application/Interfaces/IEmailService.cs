﻿using Application.Models;
using Domain.Entities;

namespace Application.Interfaces
{
    public interface IEmailService
    {
        string GetScheduleMailBody(string title = "", string speech = "", string alternativeSpeech = "", string sign = "", string mainContentLink = "", string time = "", string location = "");
        string GetMailRegisterBody(string title = "", string speech = "", string fullName = "", string alternativeSpeech = "", string sign = "", string phone = "", string email = "", string purpose = "");
        Task SendEmailContent(EmailModel mailData);
        Task SendEmailForRegistration(Customer customer);
        Task SendEmailScheduleAsync(Customer customer, Appointment appointment);
        Task SendEmailTest();
    }
}