﻿using Newtonsoft.Json;
using System.Reflection;
using System.Text;

namespace Application.Extensions
{
    public static class DataInitializer
    {
        private static readonly Assembly assembly = typeof(DataInitializer).Assembly;
        public static async Task<List<TEntity>> SeedDataAsync<TEntity>() where TEntity : class
        {

            var filePath = $"{SystemConstants.BASE_ASSEMPLY_PATH}.{typeof(TEntity).Name.ToPluralFormName()}.json";
            using var fileStream = assembly.GetManifestResourceStream(filePath) ?? throw new Exception($" Path is not found!");
            using var reader = new StreamReader(fileStream, Encoding.UTF8);
            var jsonString = await reader.ReadToEndAsync();
            var data = JsonConvert.DeserializeObject<List<TEntity>>(jsonString) ?? throw new Exception($"{typeof(TEntity).Name} List is empty!");
            return data;
        }
    }

}

