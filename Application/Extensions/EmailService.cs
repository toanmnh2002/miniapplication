﻿using Application.Interfaces;
using Application.Interfaces.IServices;
using Application.Models;
using Domain.Entities;
using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using System.Text;

namespace Application.Extensions
{
    public class EmailService : IEmailService
    {
        private readonly MimeMessage _email;
        private readonly BodyBuilder _builder;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IReceptionistService _receptionistService;

        public EmailService(IUnitOfWork unitOfWork, IReceptionistService receptionistService)
        {
            _email = new MimeMessage();
            _builder = new BodyBuilder();
            _unitOfWork = unitOfWork;
            _receptionistService = receptionistService;
        }

        public string GetScheduleMailBody(string title = "", string speech = "", string alternativeSpeech = "", string sign = "", string time = "", string receptionistName = "", string location = "")
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader("Data/ScheduleMail.html"))
            {
                string line = "";
                StringBuilder stringBuilder = new StringBuilder();
                while ((line = reader.ReadLine()) is not null)
                {
                    stringBuilder.Append(line);
                }
                body = stringBuilder.ToString();
            }
            body = body.Replace("{Title}", title);
            body = body.Replace("{Speech}", speech);
            body = body.Replace("{AlternativeSpeech}", alternativeSpeech);
            body = body.Replace("{Time}", time);
            body = body.Replace("{ReceptionistId}", receptionistName);
            body = body.Replace("{Sign}", sign);
            body = body.Replace("{Location}", location);
            return body;
        }

        public string GetMailRegisterBody(string title = "", string speech = "", string time = "", string alternativeSpeech = "", string sign = "", string location = "", string email = "", string purpose = "")
        {
            string body = string.Empty;
            var filePath = "Data/RegisterMail.html";
            if (!File.Exists(filePath))
            {
                throw new Exception("Can not find file");
            }
            using (StreamReader reader = new StreamReader(filePath))
            {
                string line = "";
                StringBuilder stringBuilder = new StringBuilder();
                while ((line = reader.ReadLine()) is not null)
                {
                    stringBuilder.Append(line);
                }
                body = stringBuilder.ToString();
            }
            body = body.Replace("{Title}", title);
            body = body.Replace("{Speech}", speech);
            //body = body.Replace("{fullName}", fullName);
            body = body.Replace("{AlternativeSpeech}", alternativeSpeech);
            //body = body.Replace("{Phone}", phone);
            body = body.Replace("{Email}", email);
            body = body.Replace("{Purpose}", purpose);
            body = body.Replace("{Sign}", sign);
            return body;
        }

        public async Task SendEmailContent(EmailModel mailData)
        {
            #region Sender & Receiver
            //sender
            _email.From.Add(new MailboxAddress(EmailSettings.DISPLAYNAME, mailData.From ?? EmailSettings.FROM));
            _email.Sender = new MailboxAddress(mailData.DisplayName ?? EmailSettings.DISPLAYNAME, mailData.From ?? EmailSettings.FROM);

            //receiver
            foreach (var mailAddress in mailData.To)
            {
                _email.To.Add(MailboxAddress.Parse(mailAddress));
            }
            //set Reply to if specified in request
            if (!string.IsNullOrEmpty(mailData.ReplyTo))
            {
                _email.ReplyTo.Add(new MailboxAddress(mailData.ReplyToName, mailData.ReplyTo));
            }

            // BCC
            // Check if a BCC was supplied in the request
            if (mailData.Bcc != null)
            {
                // Get only addresses where value is not null or with whitespace. x = value of address
                foreach (string mailAddress in mailData.Bcc.Where(x => !string.IsNullOrWhiteSpace(x)))
                    _email.Bcc.Add(MailboxAddress.Parse(mailAddress.Trim()));
            }

            // CC
            // Check if a CC address was supplied in the request
            if (mailData.Cc != null)
            {
                foreach (string mailAddress in mailData.Cc.Where(x => !string.IsNullOrWhiteSpace(x)))
                    _email.Cc.Add(MailboxAddress.Parse(mailAddress.Trim()));
            }
            #endregion

            #region Content

            //add content to mime message
            _email.Subject = mailData.Subject;
            _builder.HtmlBody = mailData.Body;
            _email.Body = _builder.ToMessageBody();
            #endregion

            using var smtp = new SmtpClient();
            await smtp.ConnectAsync(EmailSettings.PROVIDER, EmailSettings.PORT, SecureSocketOptions.StartTls);
            await smtp.AuthenticateAsync(EmailSettings.USERNAME, EmailSettings.PASSWORD);
            await smtp.SendAsync(_email);
            await smtp.DisconnectAsync(true);
            await Console.Out.WriteLineAsync("send email success");
        }
        public async Task SendEmailScheduleAsync(Customer customer, Appointment appointment)
        {
            var to = new List<string> { customer.Email };
            var title = "Chào mừng tới với ToanNguyen System";
            var speech = "Thân gửi bạn,\nChào mừng bạn đã tham gia hệ thống. Đây là thông tin cá nhân của bạn:";
            var alternativeSpeech = "chào bạn chúng tôi kính gửi bạn lịch trình";
            var sign = "ToanNguyenManh";

            var time = appointment.Date.ToString("dd/MM/yyy");
            var location = appointment.Location;
            var receptionistObj = _receptionistService.GetReceptionistById(appointment.ReceptionistId);
            var receptionistId = receptionistObj.Name;

            var body = GetScheduleMailBody(title, speech, alternativeSpeech, sign, time, location, receptionistId);
            var subject = "Chào mừng tới với hệ thống ToanNguyen";
            var mailData = new EmailModel(to, subject, body);
            await SendEmailContent(mailData);
        }

        public async Task SendEmailForRegistration(Customer customer)
        {

            var to = new List<string> { customer.Email };

            var title = "Chào mừng tới với ToanNguyen System";
            var speech = "hello cưng";
            var alternativeSpeech = "hic";
            var sign = "ToanNguyenManh";

            var fullname = customer.FullName;
            var phone = customer.Phone;
            var email = customer.Email;
            var purpose = customer.Purpose;

            var body = GetMailRegisterBody(title, speech, fullname, alternativeSpeech, sign, phone, email, purpose);
            var subject = "Chào mừng tới với hệ thống ToanNguyen";

            var mailData = new EmailModel(to, subject, body);
            await SendEmailContent(mailData);

        }

        public async Task SendEmailTest()
        {

            var to = new List<string> { "toanmanh2002@gmail.com" };

            var title = "Chào mừng tới với ToanNguyen System";
            var speech = "hello cưng";
            var alternativeSpeech = "hic";
            var sign = "ToanNguyenManh";

            var body = GetMailRegisterBody(title, speech, alternativeSpeech, sign);
            var subject = "Chào mừng tới với hệ thống ToanNguyen";

            var mailData = new EmailModel(to, subject, body);
            await SendEmailContent(mailData);

        }

    }
}
