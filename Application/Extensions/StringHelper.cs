﻿namespace Application.Extensions
{
    public static class StringHelper
    {
        public static string ToPluralFormName(this string name)
        {
            var PLURAL_FORM_ES = new string[] { "o", "s", "ch", "x", "sh", "z" };

            var lastCharsInName = name[^2..];

            var correctPluralForm = PLURAL_FORM_ES.Any(x => lastCharsInName.EndsWith(x)) ? "es" : "s";

            return name + correctPluralForm;
        }

    }
}
