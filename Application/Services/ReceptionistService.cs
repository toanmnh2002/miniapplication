﻿using Application.Interfaces;
using Application.Interfaces.IServices;
using Domain.Entities;

namespace Application.Services
{
    public class ReceptionistService : IReceptionistService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ReceptionistService(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public Receptionist GetReceptionistById(int id) => _unitOfWork.ReceptionistRepository.GetReceptionistById(id);
    }
}
