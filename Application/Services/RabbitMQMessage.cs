﻿using Application.Interfaces.IServices;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System.Text;

namespace Application.Services
{
    public class RabbitMQMessage : IMessageProducer
    {
        public void SendMessages<T>(T message)
        {
            var connectionFactory = new ConnectionFactory
            {
                HostName = "localhost",
                Port = 5673,
                UserName = "toannguyen",
                Password = "123",
                VirtualHost = "/",
            };

            var connection = connectionFactory.CreateConnection();
            using var channel = connection.CreateModel();

            channel.QueueDeclare(queue: "customerTest",
                                 exclusive: false);

            var jsonData = JsonConvert.SerializeObject(message);
            var body = Encoding.UTF8.GetBytes(jsonData);
            channel.BasicPublish(exchange: "",
                                     routingKey: "customerTest",
                                     basicProperties: null,
                                     body: body);
        }
    }
}

