﻿using Application.Interfaces;
using Application.Interfaces.IServices;
using Domain.Entities;

namespace Application.Services
{
    public class AppointMentService : IAppointmentService
    {
        private readonly IUnitOfWork _unitOfWork;

        public AppointMentService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public bool AddAppointment(Appointment appointment)
        {
            _unitOfWork.AppointmentRepository.AddEntity(appointment);

            var isSuccess = _unitOfWork.SaveChanges() > 0;
            return isSuccess;
        }

        public IEnumerable<Appointment> ShowAllAppointment() => _unitOfWork.AppointmentRepository.GetAll();

        public Appointment GetAppointmentById(int id) => _unitOfWork.AppointmentRepository.GetById(id) ?? throw new Exception("Appointment is not existed!");
    }

}

