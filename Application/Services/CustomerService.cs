﻿using Application.Interfaces;
using Application.Interfaces.IServices;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Application.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IEmailService _emailService;
        private readonly IMessageProducer _messageProducer;

        public CustomerService(IUnitOfWork unitOfWork, IEmailService emailService, IMessageProducer messageProducer)
        {
            _unitOfWork = unitOfWork;
            _emailService = emailService;
            _messageProducer = messageProducer;
        }

        public bool AddCustomer(Customer customer)
        {
            _unitOfWork.CustomerRepository.AddEntity(customer);
            _emailService.SendEmailForRegistration(customer);

            var isSuccess = _unitOfWork.SaveChanges() > 0;
            _messageProducer.SendMessages(customer);
            return isSuccess;
        }

        public bool DeleteCustomer(Customer customer)
        {
            _unitOfWork.CustomerRepository.Remove(customer);

            var isSuccess = _unitOfWork.SaveChanges() > 0;
            return isSuccess;
        }

        public bool UpdateCustomer(Customer customer)
        {
            _unitOfWork.CustomerRepository.Update(customer);

            var isSuccess = _unitOfWork.SaveChanges() > 0;
            return isSuccess;
        }

        public Customer GetCustomerById(int id) => _unitOfWork.CustomerRepository.GetEntityByCondition(x => x.Id == id);

        public IEnumerable<Customer> ShowAllCustomer() => _unitOfWork.CustomerRepository.GetAll();

        public IEnumerable<Customer> ShowAppointmentCustomer() => _unitOfWork.CustomerRepository.Filter(include: x => x.Include(x => x.Appointments));
    }

}

