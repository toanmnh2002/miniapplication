﻿using Application.Extensions;
using Application.Interfaces;
using Application.Interfaces.IMenus;
using Application.Interfaces.IServices;
using Application.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MiniProject.View;
using MiniProject.View.MiniProject.View;
using Persistence;

namespace MiniProject.Containers
{
    public static class DependencyInjection
    {
        public static IServiceCollection RegisterService(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IReceptionistService, ReceptionistService>();
            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<IAppointmentService, AppointMentService>();
            services.AddScoped<IMessageProducer, RabbitMQMessage>();

            services.AddScoped<IMenuBase, MenuBase>();
            services.AddScoped<IMenuCustomer, MenuCustomer>();
            services.AddScoped<IMenuReceptionist, MenuReceptionist>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddDbContext<AppDbContext>(options =>
            options.UseSqlServer(configuration.GetConnectionString("db")));

            return services;
        }
    }
}
