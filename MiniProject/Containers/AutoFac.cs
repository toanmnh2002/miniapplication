﻿using Application.Extensions;
using Application.Interfaces;
using Application.Interfaces.IMenus;
using Application.Interfaces.IServices;
using Application.Services;
using Autofac;
using MiniProject.View;
using MiniProject.View.MiniProject.View;
using Persistence;

namespace MiniProject.Containers
{
    public static class AutoFac
    {
        public static void RegisterService(ContainerBuilder builder, AppDbContext context)
        {
            // Register services
            builder.Register((a) => { return context; }).As<AppDbContext>();

            builder.RegisterType<EmailService>().As<IEmailService>().InstancePerLifetimeScope();
            builder.RegisterType<CustomerService>().As<ICustomerService>().InstancePerLifetimeScope();
            builder.RegisterType<AppointMentService>().As<IAppointmentService>().InstancePerLifetimeScope();
            builder.RegisterType<ReceptionistService>().As<IReceptionistService>().InstancePerLifetimeScope();

            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();
            builder.RegisterType<MenuBase>().As<IMenuBase>().InstancePerLifetimeScope();
            builder.RegisterType<MenuReceptionist>().As<IMenuReceptionist>().InstancePerLifetimeScope();
            builder.RegisterType<MenuCustomer>().As<IMenuCustomer>().InstancePerLifetimeScope();
        }
    }
}
