﻿using Application.Interfaces.IMenus;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MiniProject.Containers;
using Persistence;

#region DependencyInjection
IConfiguration configuration = new ConfigurationBuilder()
    .SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json")
    .Build();

var dbContextFactory = new AppDbContextFactory();
var context = dbContextFactory.CreateDbContext(args);
IServiceCollection services = new ServiceCollection();
services.RegisterService(configuration);

var serviceProvider = services.BuildServiceProvider();

// Resolve the root menu and start the application
using (var scope = serviceProvider.CreateScope())
{
    var menuBase = scope.ServiceProvider.GetService<IMenuBase>();
    await SeedData.DataInitialization(context);
    menuBase.HandleBaseMenu();
}
#endregion


#region Autofac
//var builder = new ContainerBuilder();
//AutoFac.RegisterService(builder, context);
//// Register services
//var container = builder.Build();

//// Resolve the root menu and start the application
//using (var scope = container.BeginLifetimeScope())
//{
//    var menuBase = scope.Resolve<IMenuBase>();
//    await SeedData.DataInitialization(context);
//    menuBase.HandleBaseMenu();
//}
#endregion



