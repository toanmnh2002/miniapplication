﻿namespace MiniProject.View
{
    using Application.Interfaces;
    using Application.Interfaces.IMenus;
    using Application.Interfaces.IServices;

    namespace MiniProject.View
    {
        public class MenuCustomer : IMenuCustomer
        {
            private readonly ICustomerService _customerService;
            private readonly IEmailService _emailService;

            public MenuCustomer(ICustomerService customerService, IEmailService emailService)
            {
                _customerService = customerService;
                _emailService = emailService;
            }

            public void HandleMenu()
            {
                bool back = false;
                Console.OutputEncoding = System.Text.Encoding.UTF8;
                do
                {

                    CustomerMenu();
                    Console.Write("Nhập số tương ứng: ");
                    int.TryParse(Console.ReadLine(), out int choice);

                    switch (choice)
                    {
                        case 1:
                            HandleShowCustomerById();
                            break;
                        case 2:
                            back = !MenuBase.ConfirmYesNo("Bạn muốn tiếp tục chứ(y/n)?: ");
                            break;
                        default:
                            throw new Exception("Lựa chọn không hợp lệ");
                    }

                } while (!back);
            }
            public void CustomerMenu()
            {
                Console.WriteLine("───────────────────────────────────────────────────────────────");
                Console.WriteLine("                         CHÀO KHÁCH HÀNG                         ");
                Console.WriteLine("───────────────────────────────────────────────────────────────");
                Console.WriteLine("1. Xem thông tin cá nhân");
                Console.WriteLine("2. Thoát");
                Console.WriteLine("───────────────────────────────────────────────────────────────");
            }

            public void HandleShowCustomerById()
            {
                Console.Write("Nhập id của bạn: ");
                _ = int.TryParse(Console.ReadLine(), out int id);

                var customer = _customerService.GetCustomerById(id) ?? throw new Exception($"Customer is not existed!");

                Console.WriteLine("-----------------------------------------------------------------------------------------------------");
                Console.WriteLine("| CustomerId |     Full Name      |   Phone Number   |    Email              |     Purpose          |");
                Console.WriteLine("-----------------------------------------------------------------------------------------------------");
                Console.WriteLine($"| {customer.Id,-11} | {customer.FullName,-20} | {customer.Phone,-16} | {customer.Email,-24} | {customer.Purpose ?? "Chưa có dữ liệu",-20} |");
                Console.WriteLine("-----------------------------------------------------------------------------------------------------");
            }
        }
    }

}
