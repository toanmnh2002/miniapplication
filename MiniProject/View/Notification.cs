﻿namespace MiniProject.View
{
    public static class Notification
    {
        public static void Response(string message)
        {
            if (message.Contains("thành công"))
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(message);
                Console.ResetColor();
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(message);
                Console.ResetColor();
            }
        }

    }
}
