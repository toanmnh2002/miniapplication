﻿using Application.Interfaces.IMenus;
using System.Text;

namespace MiniProject.View
{
    public class MenuBase(IMenuCustomer menuCustomer, IMenuReceptionist menuReceptionist) : IMenuBase
    {
        private readonly IMenuCustomer _menuCustomer = menuCustomer;
        private readonly IMenuReceptionist _menuReceptionist = menuReceptionist;
        public void HandleBaseMenu()
        {
            Console.OutputEncoding = Encoding.UTF8;
            bool back = false;
            do
            {
                try
                {
                    BaseMenu();
                    int.TryParse(Console.ReadLine(), out int choice);

                    switch (choice)
                    {
                        case 1:
                            _menuCustomer.HandleMenu();
                            break;
                        case 2:
                            _menuReceptionist.HandleMenu();
                            break;
                        case 3:
                            back = !ConfirmYesNo("Bạn muốn tiếp tục chứ(y/n)?: ");
                            break;
                        default:
                            throw new Exception("Lựa chọn không hợp lệ");
                    }

                }

                catch (Exception ex)
                {
                    Notification.Response(ex.Message);
                }

            } while (!back);
        }

        public static void BaseMenu()
        {
            Console.WriteLine("──────────────────────────────────────");
            Console.WriteLine("             HỆ THỐNG                 ");
            Console.WriteLine("──────────────────────────────────────");
            Console.WriteLine("1. Khách hàng");
            Console.WriteLine("2. Nhân viên");
            Console.WriteLine("3. Thoát");
            Console.WriteLine("──────────────────────────────────────");
            Console.Write("Nhập số tương ứng: ");
        }


        #region ConfirmYesNo
        public static bool ConfirmYesNo(string message)
        {
            Console.Write(message);
            string answer = Console.ReadLine();
            if (string.IsNullOrEmpty(answer))
            {
                throw new Exception("Invalid input!");
            }
            if (answer.Equals("yes", StringComparison.OrdinalIgnoreCase) || answer.Equals("y", StringComparison.OrdinalIgnoreCase)) return true;
            if (answer.Equals("no", StringComparison.OrdinalIgnoreCase) || answer.Equals("n", StringComparison.OrdinalIgnoreCase)) return false;
            return false;
        }
        #endregion
    }

}

