﻿using Application.FluentValidation;
using Application.Interfaces;
using Application.Interfaces.IMenus;
using Application.Interfaces.IServices;
using Domain.Entities;
using FluentValidation;

namespace MiniProject.View
{

    public class MenuReceptionist : IMenuReceptionist
    {
        private readonly ICustomerService _customerService;
        private readonly IAppointmentService _appointmentService;
        private readonly IReceptionistService _receptionistService;
        private readonly IValidator<Customer> _customerValidator;
        private readonly IValidator<Appointment> _appointmentValidator;
        private readonly IEmailService _emailService;

        public MenuReceptionist(ICustomerService customerService, IAppointmentService appointmentService, IReceptionistService receptionistService, IEmailService emailService)
        {
            _customerService = customerService;
            _appointmentService = appointmentService;
            _receptionistService = receptionistService;
            _appointmentValidator = new AppointmentValidation();
            _customerValidator = new CustomerValidation();
            _emailService = emailService;
        }

        public void HandleMenu()
        {
            bool back = false;
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            do
            {

                ReceptionistMenu();
                Console.Write("Nhập số tương ứng: ");
                int.TryParse(Console.ReadLine(), out int choice);

                switch (choice)
                {
                    case 1:
                        HandleSchedule();
                        break;
                    case 2:
                        HandleCustomer();
                        break;
                    case 3:
                        back = !MenuBase.ConfirmYesNo("Bạn muốn tiếp tục chứ?(y/n): ");
                        break;
                    default:
                        throw new Exception("Lựa chọn không hợp lệ");
                }

            } while (!back);
        }
        public void ReceptionistMenu()
        {
            Console.WriteLine("───────────────────────────────────────");
            Console.WriteLine("           NHÂN VIÊN LỄ TÂN             ");
            Console.WriteLine("───────────────────────────────────────");
            Console.WriteLine("1. Quản lý lịch tập");
            Console.WriteLine("2. Quản lý khách hàng");
            Console.WriteLine("3. Thoát");
            Console.WriteLine("───────────────────────────────────────");
        }

        #region ScheduleUI&Input
        public void HandleSchedule()
        {
            bool back = false;
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            do
            {

                ScheduleManager();
                Console.Write("Nhập số tương ứng: ");
                int.TryParse(Console.ReadLine(), out int choice);

                switch (choice)
                {
                    case 1:
                        HandleAddSchedule();
                        break;
                    case 2:
                        HandleShowAllApointment();
                        break;
                    case 3:
                        HandleSendScheduleEmail();
                        break;
                    case 4:
                        back = !MenuBase.ConfirmYesNo("Bạn muốn tiếp tục chứ(y/n)?: ");
                        break;
                    default:
                        throw new Exception("Lựa chọn không hợp lệ");
                }

            } while (!back);
        }
        public void ScheduleManager()
        {
            Console.WriteLine("───────────────────────────────────────");
            Console.WriteLine("          QUẢN LÝ LỊCH HẸN             ");
            Console.WriteLine("───────────────────────────────────────");
            Console.WriteLine("1. Thêm lịch hẹn");
            Console.WriteLine("2. Xem lịch hẹn");
            Console.WriteLine("3. Gửi email thông báo lịch tập đến cho khách hàng");
            Console.WriteLine("4. Thoát");
            Console.WriteLine("───────────────────────────────────────");
        }

        public void HandleAddSchedule()
        {
            Console.Write("Nhập Id của bạn: ");
            var checkReceptionistId = int.TryParse(Console.ReadLine(), out int receptionistId);

            Console.Write("Nhập Id của khách hàng: ");
            var checkCustomerId = int.TryParse(Console.ReadLine(), out int customerId);

            Console.Write("Nhập địa điểm: ");
            var location = Console.ReadLine();

            var appointment = new Appointment
            {
                ReceptionistId = checkReceptionistId ? receptionistId : throw new Exception("Invalid for receptionId "),
                CustomerId = checkCustomerId ? customerId : throw new Exception("Invalid for customerId"),
                Date = DateTime.Now,
                Location = location,
            };

            _appointmentValidator.ValidateAndThrow(appointment);

            _appointmentService.AddAppointment(appointment);
            var isSuccess = _appointmentService.AddAppointment(appointment) ? $"Add {nameof(appointment)} sucessfully!" : $"Add {nameof(appointment)} fail!";
            Notification.Response(isSuccess);
        }

        public async void HandleSendScheduleEmail()
        {
            var allCustomers = _customerService.ShowAllCustomer();
            var allAppointments = _appointmentService.ShowAllAppointment();
            var appointmentsQuery = allCustomers
                .Join(allAppointments,
                    customer => customer.Id,
                    appointment => appointment.CustomerId,
                    (customer, appointment) => new { Customer = customer, Appointment = appointment })
                .GroupBy(x => x.Customer)
                .Select(g => new { Customer = g.Key, Appointments = g.Select(x => x.Appointment).ToList() });

            foreach (var customerAppointments in appointmentsQuery)
            {
                var customer = customerAppointments.Customer;
                var appointments = customerAppointments.Appointments;
                foreach (var appointment in appointments)
                {
                    await _emailService.SendEmailScheduleAsync(customer, appointment);
                }
            }
        }

        public void HandleShowAllApointment()
        {
            var listAppointment = _appointmentService.ShowAllAppointment();

            Console.WriteLine("---------------------------------------------------------------------------------");
            Console.WriteLine("| CustomerId Name |     Receptionist Name      |   Date   |    Location    |");
            Console.WriteLine("---------------------------------------------------------------------------------");

            foreach (var appointment in listAppointment)
            {
                var customer = _customerService.GetCustomerById(appointment.CustomerId);
                var receptionist = _receptionistService.GetReceptionistById(appointment.ReceptionistId);
                Console.WriteLine($"| {customer.FullName,-11} | {receptionist.Name,-20} | {appointment.Date,-16} | {appointment.Location,-20} |");
            }

            Console.WriteLine("---------------------------------------------------------------------------------");
        }
        #endregion


        #region CustomerUI&Input
        public void HandleCustomer()
        {
            bool back = false;
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            try
            {
                do
                {

                    CustomerManager();
                    Console.Write("Nhập số tương ứng: ");
                    int.TryParse(Console.ReadLine(), out int choice);

                    switch (choice)
                    {
                        case 1:
                            HandleAddCustomer();
                            break;
                        case 2:
                            HandleUpdateCustomer();
                            break;
                        case 3:
                            HandleDeleteCustomer();
                            break;
                        case 4:
                            HandleShowAllCustomer();
                            break;
                        case 5:
                            HandleShowCustomerById();
                            break;
                        case 6:
                            HandleShowAppointmentCustomer();
                            break;
                        case 7:
                            back = !MenuBase.ConfirmYesNo("Bạn muốn tiếp tục chứ(y/n)?: ");
                            break;
                        default:
                            throw new Exception("Lựa chọn không hợp lệ");

                    }
                } while (!back);
            }
            catch (Exception ex)
            {
                Notification.Response(ex.Message);
            }

        }
        public void CustomerManager()
        {
            Console.WriteLine("───────────────────────────────────────");
            Console.WriteLine("         QUẢN LÝ KHÁCH HÀNG           ");
            Console.WriteLine("───────────────────────────────────────");
            Console.WriteLine("1. Thêm khách hàng");
            Console.WriteLine("2. Sửa khách hàng");
            Console.WriteLine("3. Xóa khách hàng");
            Console.WriteLine("4. Xem thông tin khách hàng");
            Console.WriteLine("5. Xem thông tin cá nhân của một khách hàng");
            Console.WriteLine("6. Xem thông tin khách hàng và lịch hẹn của họ");
            Console.WriteLine("7. Thoát");
            Console.WriteLine("───────────────────────────────────────");
        }
        public void HandleAddCustomer()
        {
            Console.Write("Nhập tên của khách hàng: ");
            var name = Console.ReadLine();
            Console.Write("Nhập địa chỉ Email của khách hàng: ");
            var email = Console.ReadLine();
            Console.Write("Nhập số điện thoại của khách hàng: ");
            var phone = Console.ReadLine();
            Console.Write("Nhập mục đích đến của khách hàng: ");
            var purpose = Console.ReadLine();


            var customer = new Customer
            {
                FullName = name,
                Email = email,
                Phone = phone,
                Purpose = purpose
            };

            _customerValidator.ValidateAndThrow(customer);

            var isSuccess = _customerService.AddCustomer(customer) ? $"Add {nameof(customer)} sucessfully!" : $"Add {nameof(customer)} fail!";
            Notification.Response(isSuccess);
        }

        public void HandleUpdateCustomer()
        {
            Console.Write("Nhập id của khách hàng: ");
            _ = int.TryParse(Console.ReadLine(), out int id);

            var customer = _customerService.GetCustomerById(id) ?? throw new Exception("Customer is not existed!");

            Console.Write("Nhập tên của khách hàng: ");
            var name = Console.ReadLine();
            Console.Write("Nhập địa chỉ Email của khách hàng: ");
            var email = Console.ReadLine();
            Console.Write("Nhập số điện thoại của khách hàng: ");
            var phone = Console.ReadLine();
            Console.Write("Nhập mục đích đến của khách hàng: ");
            var purpose = Console.ReadLine();

            if (!string.IsNullOrWhiteSpace(name)) customer.FullName = name;
            if (!string.IsNullOrWhiteSpace(email)) customer.Email = email;
            if (!string.IsNullOrWhiteSpace(phone)) customer.Phone = phone;
            if (!string.IsNullOrWhiteSpace(purpose)) customer.Purpose = purpose;

            _customerValidator.ValidateAndThrow(customer);

            var isSucess = _customerService.UpdateCustomer(customer) ? $"Cập nhật {nameof(customer)} thành công!" : $"Cập nhật {nameof(customer)} thất bại!";
            Notification.Response(isSucess);

        }

        public void HandleDeleteCustomer()
        {
            Console.Write("Nhập id của khách hàng: ");
            _ = int.TryParse(Console.ReadLine(), out int id);

            var customer = _customerService.GetCustomerById(id) ?? throw new Exception("Customer is not existed!");
            _customerService.DeleteCustomer(customer);
            var isSucess = _customerService.DeleteCustomer(customer) ? $"Xóa {nameof(customer)} thành công!" : $"Xóa {nameof(customer)} thất bại!";
            Notification.Response(isSucess);

        }

        public void HandleShowAllCustomer()
        {
            var list = _customerService.ShowAllCustomer();

            Console.WriteLine("-----------------------------------------------------------------------------------------------------");
            Console.WriteLine("| CustomerId |     Full Name      |   Phone Number   |    Email              |     Purpose          |");
            Console.WriteLine("-----------------------------------------------------------------------------------------------------");

            foreach (var customer in list)
            {
                Console.WriteLine($"| {customer.Id,-11} | {customer.FullName,-20} | {customer.Phone,-16} | {customer.Email,-24} | {customer.Purpose ?? "Chưa có dữ liệu"} |");
            }
            Console.WriteLine("-----------------------------------------------------------------------------------------------------");
        }

        public void HandleShowCustomerById()
        {
            Console.Write("Nhập id của bạn: ");
            _ = int.TryParse(Console.ReadLine(), out int id);

            var customer = _customerService.GetCustomerById(id) ?? throw new Exception($"Customer is not existed!");

            Console.WriteLine("-----------------------------------------------------------------------------------------------------");
            Console.WriteLine("| CustomerId |     Full Name      |   Phone Number   |    Email              |     Purpose          |");
            Console.WriteLine("-----------------------------------------------------------------------------------------------------");
            Console.WriteLine($"| {customer.Id,-11} | {customer.FullName,-20} | {customer.Phone,-16} | {customer.Email,-24} | {customer.Purpose ?? "Chưa có dữ liệu",-20} |");
            Console.WriteLine("-----------------------------------------------------------------------------------------------------");
        }

        public void HandleShowAppointmentCustomer()
        {
            var listCustomer = _customerService.ShowAppointmentCustomer();

            if (listCustomer.Count() is 0)
            {
                throw new Exception("listCustomer is empty!");
            }

            Console.WriteLine("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|");
            Console.WriteLine("| CustomerId |     Full Name      |   Phone Number   |    Email              |     Purpose          |                                         Appointments                                                                              |");
            Console.WriteLine("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|");
            Console.WriteLine("----------------------------------------------------------------------------------------------------|           Date          |           Location           |            Receptionist Name          |");
            Console.WriteLine("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|");
            foreach (var customer in listCustomer)
            {
                _ = customer.Appointments ?? throw new Exception("List is empty!");
                foreach (var item in customer.Appointments)
                {
                    var reception = _receptionistService.GetReceptionistById(item.ReceptionistId) ?? throw new Exception("Receptionist is not existed!");
                    Console.WriteLine($"| {customer.Id,-11} | {customer.FullName,-20} | {customer.Phone,-16} | {customer.Email,-24} | {customer.Purpose ?? "Chưa có dữ liệu",-20} |   {item.Date,-23} | {item.Location,-27} | {reception.Name,-36} |");
                }

            }
            Console.WriteLine("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|");
        }
        #endregion

    }

}

