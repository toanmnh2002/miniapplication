﻿using Application.Interfaces;
using Application.Interfaces.IRepositories;
using Application.Interfaces.IServices;
using AutoFixture;
using Domain.Entities;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Moq;
using Persistence;

namespace Domain.Tests
{
    public class SetupTest : IDisposable
    {
        protected readonly Fixture _fixture;
        protected readonly Mock<IUnitOfWork> _unitOfWorkMock;
        protected readonly AppDbContext _dbContext;

        protected readonly Mock<ICustomerRepository> _customerRepositoryMock;
        protected readonly Mock<IReceptionistRepository> _receptionistRepositoryMock;
        protected readonly Mock<IAppointmentRepository> _appointmentRepositoryMock;

        protected readonly Mock<ICustomerService> _customerServiceMock;
        protected readonly Mock<IReceptionistService> _receptionistServiceMock;
        protected readonly Mock<IAppointmentService> _appointmentServiceMock;

        protected readonly Mock<IValidator<Customer>> _customerValidator;
        protected readonly Mock<IValidator<Appointment>> _appointmnentValidator;
        protected readonly Mock<AppDbContext> _appDbContextMock;


        public SetupTest()
        {
            _fixture = new Fixture();
            _fixture.Behaviors.Add(new OmitOnRecursionBehavior());

            _unitOfWorkMock = new Mock<IUnitOfWork>();

            _customerRepositoryMock = new Mock<ICustomerRepository>();
            _receptionistRepositoryMock = new Mock<IReceptionistRepository>();
            _appointmentRepositoryMock = new Mock<IAppointmentRepository>();

            _customerServiceMock = new Mock<ICustomerService>();
            _receptionistServiceMock = new Mock<IReceptionistService>();
            _appointmentServiceMock = new Mock<IAppointmentService>();

            _customerValidator = new Mock<IValidator<Customer>>();

            _appointmnentValidator = new Mock<IValidator<Appointment>>();

            _appDbContextMock = new Mock<AppDbContext>();
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseInMemoryDatabase(databaseName: "hello".ToString())
                .Options;
            _dbContext = new AppDbContext(options);
        }

        public void Dispose() => _dbContext.Dispose();
    }
}
