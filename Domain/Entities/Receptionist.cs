﻿namespace Domain.Entities
{
    public class Receptionist
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string Position { get; set; } = null!;
        public ICollection<Appointment>? Appointments { get; set; }
    }
}
