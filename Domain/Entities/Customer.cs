﻿namespace Domain.Entities
{
    public class Customer
    {
        public int Id { get; set; }
        public string FullName { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string Phone { get; set; } = null!;
        public string? Purpose { get; set; }
        public ICollection<Appointment>? Appointments { get; set; }
    }
}
