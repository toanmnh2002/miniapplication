﻿namespace Domain.Entities
{
    public class Appointment
    {
        public DateTime Date { get; set; }
        public string? Location { get; set; }
        public int ReceptionistId { get; set; }
        public int CustomerId { get; set; }
        public Receptionist? Receptionist { get; set; }
        public Customer? Customer { get; set; }
    }
}
