using Application.Interfaces;
using Application.Interfaces.IRepositories;
using Application.Services;
using Domain.Entities;
using Moq;

namespace Application.Tests
{
    public class CustomerServiceTests
    {
        private readonly Mock<IUnitOfWork> _unitOfWorkMock;
        private readonly Mock<IEmailService> _emailServiceMock;
        private readonly CustomerService _customerService;

        public CustomerServiceTests()
        {
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _emailServiceMock = new Mock<IEmailService>();
            var customerRepositoryMock = new Mock<ICustomerRepository>();
            _unitOfWorkMock.Setup(u => u.CustomerRepository).Returns(customerRepositoryMock.Object);
            _customerService = new CustomerService(_unitOfWorkMock.Object, _emailServiceMock.Object);
        }

        [Fact]
        public void AddCustomer_ShouldReturnTrue_WhenSaveChangesIsSuccessful()
        {
            // Arrange
            var customer = new Customer();
            _unitOfWorkMock.Setup(u => u.SaveChanges()).Returns(1);

            // Act
            var result = _customerService.AddCustomer(customer);

            // Assert
            Assert.True(result);
            _unitOfWorkMock.Verify(u => u.CustomerRepository.AddEntity(customer), Times.Once);
            _emailServiceMock.Verify(e => e.SendEmailForRegistration(customer), Times.Once);
        }

        // Add more tests for DeleteCustomer, UpdateCustomer, GetCustomerById, ShowAllCustomer, ShowAppointmentCustomer
        [Fact]
        public void DeleteCustomer_ShouldReturnTrue_WhenSaveChangesIsSuccessful()
        {
            // Arrange
            var customer = new Customer();
            _unitOfWorkMock.Setup(u => u.SaveChanges()).Returns(1);

            // Act
            var result = _customerService.DeleteCustomer(customer);

            // Assert
            Assert.True(result);
            _unitOfWorkMock.Verify(u => u.CustomerRepository.Remove(customer), Times.Once);
        }
        [Fact]
        public void UpdateCustomer_ShouldReturnTrue_WhenSaveChangesIsSuccessful()
        {
            // Arrange
            var customer = new Customer();
            _unitOfWorkMock.Setup(u => u.SaveChanges()).Returns(1);

            // Act
            var result = _customerService.UpdateCustomer(customer);

            // Assert
            Assert.True(result);
            _unitOfWorkMock.Verify(u => u.CustomerRepository.Update(customer), Times.Once);
        }
    }
}