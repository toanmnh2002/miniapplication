﻿using Application.Interfaces;
using AutoFixture;
using Domain.Entities;
using Domain.Tests;
using FluentAssertions;

namespace Persistence.Tests
{
    public class UnitOfWorkTests : SetupTest
    {
        private readonly IUnitOfWork _unitOfWork;
        public UnitOfWorkTests()
        {
            _unitOfWork = new UnitOfWork(
                _dbContext
                , _customerRepositoryMock.Object
                , _appointmentRepositoryMock.Object
                , _receptionistRepositoryMock.Object);

        }

        [Fact]
        public void TestUnitOfWork_Customer()
        {
            // arrange
            var mockData = _fixture.Build<Customer>()
                                   .Without(x => x.Appointments)
                                   .CreateMany(10)
                                   .ToList();

            _customerRepositoryMock.Setup(x => x.GetAll()).Returns(mockData);

            // act
            var items = _unitOfWork.CustomerRepository.GetAll();

            // assert
            items.Should().BeEquivalentTo(mockData);
        }

        [Fact]
        public void TestUnitOfWork_Receptionist()
        {
            // arrange
            var mockData = _fixture.Build<Receptionist>()
                                   .Without(x => x.Appointments)
                                   .CreateMany(10)
                                   .ToList();

            _receptionistRepositoryMock.Setup(x => x.GetAll()).Returns(mockData);

            // act
            var items = _unitOfWork.ReceptionistRepository.GetAll();

            // assert
            items.Should().BeEquivalentTo(mockData);
        }

        [Fact]
        public void TestUnitOfWork_Appointment()
        {
            // arrange
            var mockData = _fixture.Build<Appointment>()
                                   .CreateMany(10)
                                   .ToList();

            _appointmentRepositoryMock.Setup(x => x.GetAll()).Returns(mockData);

            // act
            var items = _unitOfWork.AppointmentRepository.GetAll();

            // assert
            items.Should().BeEquivalentTo(mockData);
        }

        [Fact]
        public void SaveChanges_WhenCalled_ReturnsNumberOfChanges()
        {
            // Arrange
            var expectedChanges = 0;
            _appDbContextMock.Setup(x => x.SaveChanges()).Returns(expectedChanges);

            // Act
            var result = _unitOfWork.SaveChanges();

            // Assert
            result.Should().Be(expectedChanges);
        }
    }
}
