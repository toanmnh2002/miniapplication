﻿using Application.Interfaces.IRepositories;
using AutoFixture;
using Domain.Entities;
using Domain.Tests;
using FluentAssertions;
using Persistence.Repositories;

namespace Persistence.Tests
{
    public class ReceptionistRepositoryTests : SetupTest
    {
        private readonly IReceptionistRepository _receptionistRepository;

        public ReceptionistRepositoryTests() => _receptionistRepository = new ReceptionistRepository(_dbContext);

        [Fact]
        public void GetReceptionistById_Should_ReturnTrue()
        {
            // arrange
            var mockData = _fixture.Build<Receptionist>()
                                   .With(x => x.Id)
                                   .Without(x => x.Appointments)
                                   .Create();

            _dbContext.Receptionist.AddAsync(mockData);
            _dbContext.SaveChangesAsync();

            //act
            var result = _receptionistRepository.GetReceptionistById(mockData.Id);

            //assert
            result.Should().BeEquivalentTo(mockData);
        }

    }
}
