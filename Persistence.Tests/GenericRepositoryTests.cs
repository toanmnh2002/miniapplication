﻿using Application.Interfaces.IRepositories;
using Domain.Entities;
using Domain.Tests;
using FluentAssertions;
using Persistence.Repositories;

namespace Persistence.Tests
{


    public class GenericRepositoryTests : SetupTest
    {
        private readonly IGenericRepository<Customer> _genericRepository;

        public GenericRepositoryTests()
        {
            _genericRepository = new GenericRepository<Customer>(_dbContext);
        }

        [Fact]
        public void GenericRepository_GetById_ShouldReturnCorrectData()
        {
            // Arrange
            var customer = new Customer { FullName = "Test Customer", Email = "test@example.com", Purpose = "Test Purpose", Phone = "1234567890" };
            _dbContext.Customer.Add(customer);
            _dbContext.SaveChanges();

            // Act
            var result = _genericRepository.GetById(customer.Id);

            // Assert
            result.Should().BeEquivalentTo(customer);
        }

        [Fact]
        public void GenericRepository_GetById_ShouldReturnNullWhenNoData()
        {
            // Act
            var result = _genericRepository.GetById(0);

            // Assert
            result.Should().BeNull();
        }

        [Fact]
        public void GenericRepository_AddEntity_ShouldReturnCorrectData()
        {
            // Arrange
            var customer = new Customer { FullName = "Test Customer", Email = "toanmnh2002@gmail.com", Purpose = "fix", Phone = "123123" };

            // Act
            _genericRepository.AddEntity(customer);
            var result = _dbContext.SaveChanges();

            // Assert
            result.Should().Be(1);

            // Verify the entity was added to the context
            _dbContext.Customer.Find(customer.Id).Should().NotBeNull();
            _dbContext.Customer.Find(customer.Id).FullName.Should().Be("Test Customer");
        }

        [Fact]
        public void GenericRepository_AddRange_ShouldReturnCorrectData()
        {
            // Arrange
            var customers = new List<Customer>
            {
                new Customer { FullName = "Customer 1", Email = "customer1@example.com", Purpose = "Purpose 1", Phone = "1234567890" },
                new Customer { FullName = "Customer 2", Email = "customer2@example.com", Purpose = "Purpose 2", Phone = "0987654321" },
                new Customer { FullName = "Customer 3", Email = "customer3@example.com", Purpose = "Purpose 3", Phone = "1122334455" }
            };

            // Act
            _genericRepository.AddRange(customers);
            var result = _dbContext.SaveChanges();

            // Assert
            result.Should().Be(3);
        }

        [Fact]
        public void GenericRepository_Delete_ShouldReturnCorrectData()
        {
            // Arrange
            var customer = new Customer { FullName = "Test Customer", Email = "test@example.com", Purpose = "Test Purpose", Phone = "1234567890" };
            _dbContext.Customer.Add(customer);
            _dbContext.SaveChanges();

            // Act
            _genericRepository.Remove(customer);
            var result = _dbContext.SaveChanges();

            // Assert
            result.Should().Be(1);
        }

        [Fact]
        public void GenericRepository_Update_ShouldReturnCorrectData()
        {
            // Arrange
            var customer = new Customer { FullName = "Test Customer", Email = "test@example.com", Purpose = "Test Purpose", Phone = "1234567890" };
            _dbContext.Customer.Add(customer);
            _dbContext.SaveChanges();

            // Modify the customer data
            customer.FullName = "Updated Customer";

            // Act
            _genericRepository.Update(customer);
            var result = _dbContext.SaveChanges();

            // Assert
            result.Should().Be(1);
        }

        [Fact]
        public void GenericRepository_RemoveRange_ShouldReturnCorrectData()
        {
            // Arrange
            var customers = new List<Customer>
            {
                new Customer { FullName = "Customer 1", Email = "customer1@example.com", Purpose = "Purpose 1", Phone = "1234567890" },
                new Customer { FullName = "Customer 2", Email = "customer2@example.com", Purpose = "Purpose 2", Phone = "0987654321" }
            };
            _dbContext.Customer.AddRange(customers);
            _dbContext.SaveChanges();

            // Act
            _genericRepository.RemoveRange(customers);
            var result = _dbContext.SaveChanges();

            // Assert
            result.Should().Be(2);
        }

        [Fact]
        public void GenericRepository_UpdateRange_ShouldReturnCorrectData()
        {
            // Arrange
            var customers = new List<Customer>
            {
                new Customer { FullName = "Customer 1", Email = "customer1@example.com", Purpose = "Purpose 1", Phone = "1234567890" },
                new Customer { FullName = "Customer 2", Email = "customer2@example.com", Purpose = "Purpose 2", Phone = "0987654321" }
            };
            _dbContext.Customer.AddRange(customers);
            _dbContext.SaveChanges();

            // Modify the customers data
            customers[0].FullName = "Updated Customer 1";
            customers[1].FullName = "Updated Customer 2";

            // Act
            _genericRepository.UpdateRange(customers);
            var result = _dbContext.SaveChanges();

            // Assert
            result.Should().Be(2);
        }
    }
}

