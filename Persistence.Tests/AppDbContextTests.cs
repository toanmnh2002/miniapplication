﻿using Domain.Entities;
using Domain.Tests;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;

namespace Persistence.Tests
{
    public class AppDbContextTests : SetupTest, IDisposable
    {
        [Fact]
        public async Task AppDbContext_CustomersDbSetShouldReturnCorrectData()
        {
            // Clear existing data
            _dbContext.Customer.RemoveRange(await _dbContext.Customer.ToListAsync());
            await _dbContext.SaveChangesAsync();

            // Arrange
            var customers = new List<Customer>
            {
                new Customer { FullName = "Customer 1", Email = "customer1@example.com", Purpose = "Purpose 1", Phone = "1234567890" },
                new Customer { FullName = "Customer 2", Email = "customer2@example.com", Purpose = "Purpose 2", Phone = "0987654321" },
                new Customer { FullName = "Customer 3", Email = "customer3@example.com", Purpose = "Purpose 3", Phone = "1122334455" }
            };

            await _dbContext.Customer.AddRangeAsync(customers);
            await _dbContext.SaveChangesAsync();

            // Act
            var result = await _dbContext.Customer.ToListAsync();

            // Assert
            result.Should().BeEquivalentTo(customers);
        }

        [Fact]
        public async Task AppDbContext_ReceptionistDbSetShouldReturnCorrectData()
        {
            // Clear existing data
            _dbContext.Receptionist.RemoveRange(await _dbContext.Receptionist.ToListAsync());
            await _dbContext.SaveChangesAsync();

            // Arrange
            var receptionists = new List<Receptionist>
            {
                new Receptionist { Name = "Receptionist 1", Position = "Front Desk" },
                new Receptionist { Name = "Receptionist 2", Position = "Back Office" }
            };

            await _dbContext.Receptionist.AddRangeAsync(receptionists);
            await _dbContext.SaveChangesAsync();

            // Act
            var result = await _dbContext.Receptionist.ToListAsync();

            // Assert
            result.Should().BeEquivalentTo(receptionists);
        }

        [Fact]
        public async Task AppDbContext_CustomerDbSetShouldReturnEmptyListWhenNotHavingData()
        {
            // Clear existing data
            _dbContext.Customer.RemoveRange(await _dbContext.Customer.ToListAsync());
            await _dbContext.SaveChangesAsync();

            // Act
            var result = await _dbContext.Customer.ToListAsync();

            // Assert
            result.Should().BeEmpty();
        }

        [Fact]
        public async Task AppDbContext_AppointmentDbSetShouldReturnEmptyListWhenNotHavingData()
        {
            // Clear existing data
            _dbContext.Appointment.RemoveRange(await _dbContext.Appointment.ToListAsync());
            await _dbContext.SaveChangesAsync();

            // Act
            var result = await _dbContext.Appointment.ToListAsync();

            // Assert
            result.Should().BeEmpty();
        }

        [Fact]
        public async Task AppDbContext_ReceptionistDbSetShouldReturnEmptyListWhenNotHavingData()
        {
            // Clear existing data
            _dbContext.Receptionist.RemoveRange(await _dbContext.Receptionist.ToListAsync());
            await _dbContext.SaveChangesAsync();

            // Act
            var result = await _dbContext.Receptionist.ToListAsync();

            // Assert
            result.Should().BeEmpty();
        }
    }
}
