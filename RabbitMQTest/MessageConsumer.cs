﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;

namespace RabbitMQTest
{
    public class MessageConsumer
    {
        public void ConsumerMessages()
        {
            var connectionFactory = new ConnectionFactory
            {
                HostName = "localhost",
                Port = 5673,
                UserName = "toannguyen",
                Password = "123",
                VirtualHost = "/",
            };

            var connection = connectionFactory.CreateConnection();
            using var channel = connection.CreateModel();

            channel.QueueDeclare(queue: "customerTest",
                               exclusive: false);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (_, eventArgs) =>
            {
                var body = eventArgs.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                Console.WriteLine($"Message received:{message}");
            };
            channel.BasicConsume(queue: "customerTest", autoAck: true, consumer: consumer);
        }
    }
}
